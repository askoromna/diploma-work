﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
using System.Collections;   

namespace Householder
{
    class Manager
    {
        List<double[,]> convMatrixList = new List<double[,]>();
        SaveFileDialog saveFileDialog1 = new SaveFileDialog();


        public double alpha (double [] x)
        {
            double alpha = 0;
            for (int i = 0; i< x.Length; ++i)
            {
                alpha += Math.Pow(x[i], 2);
            }
            return Math.Sqrt(alpha);
        }

        public double [] aidVector (double [] x, int k)
        {
            for (int i = 0; i <k; ++i)
            {
                x[i] = 0;
            }
            x[k]= x[k] - alpha(x);
            return x;
        }

        public double lambdaSquared (double [] x, int k )
        {
            double lambda = 0;
            double[] u = aidVector(x,k);
            for (int i = 0; i< u.Length; ++i)
            {
                lambda += u[i] * u[i];
            }
            return lambda;
        }

        public double [,] W(double[] x)
        {
            int n = x.Length;
           
            double[,] w = new double[n, n];
            for (int i = 0; i < n; ++i)
            {
                for (int j = 0; j < n; ++j)
                {
                    w[i, j] = x[i] * x[j];
                }

            }
            return w;
        }

        public double [,] conversionMatrix(double []x, int k)
        {
            double[,] E = new double[x.Length,x.Length]; // Fillin the E-matrix
            for (int i = 0; i < x.Length; ++i)
            {
                for (int j = 0; j< x.Length; ++j)
                {
                    E[i, j] = 0;
                }
                E[i, i] = 1;
            }

            double lambda = lambdaSquared(x, k);
           
            double[,] w = W(x);

            for (int i = 0; i < x.Length; ++i)
            {
                for (int j = 0; j < x.Length; ++j)
                {
                    E[i, j] -= 2 * w[i, j]/lambda;
                } 
            }
            return E;
        }

        public double [,] conversion(double [,]A, double [] x, int k)
        {
            double [,] P = conversionMatrix(x, k);
            convMatrixList.Add(P);
            return matrixMultiplication(P, A);
        }

        public double [,] reflections (double [,] A)
        {
            int m = A.GetLength(0);
            int n = A.GetLength(1);

            double[] x  = new double[m];
            

            for (int i = 0; i< n-1; ++i)
            {
                for (int j =0; j<m; ++j)
                {
                    x[j] = A[j, i];
                }

                A = conversion(A, x, i);
            }
            return A;
        }

        public double [,] matrixMultiplication (double [,] A, double [,] B)
        {
            double[,] product = new double[A.GetLength(1), A.GetLength(1)];
            for (int i = 0; i < A.GetLength(0); ++i)
            {
                for (int j = 0; j < B.GetLength(1); ++j)
                {
                    for (int inner = 0; inner < A.GetLength(1); ++inner)
                    {
                        product[i, j] += A[i, inner] * B[inner, j];
                    }
                    
                }
            }
            return product;
        }

        public  double [] resultVector (double [] b) 
        {
            double[,] B = new double[b.Length, 1];
            for (int i = 0; i < b.Length; ++i)
            {
                B[i,0] = b[i];
            }
            for (int i =0; i< convMatrixList.Count; ++i)
            {
                B = matrixMultiplication(convMatrixList[i], B);
            }
            for (int i = 0; i < b.Length; ++i)
            {
                b[i] = B[i, 0];
                //MessageBox.Show(b[i].ToString());
            }
            return b;
        }

        public double [] reverseGauss(double [,] A, double [] b)
        {
            double[] result = new double[b.Length];
            b = resultVector(b);
            //check if there are only ZEROs in last cols
            int k = -1;
            for (int j = A.GetLength(1) - 1; j >= 0; --j)
                for (int i = 0; i < A.GetLength(0) - 1; ++i)
                {
                    if (A[i, j] == 0)
                    {
                        k = j;
                        continue;
                    }
                    else { break; }
                }

            if (k != -1)
            {
                double[,] B = new double[k, k];
                for (int i = 0; i < k; i++)
                    for (int j = 0; j < k; j++)
                        B[i, j] = A[i, j];


             ////
                for (int i = k-1; i >= 0; --i)
                {
                    double sum = 0;
                    for (int j = i +1 ; j < k; ++j)
                    {
                        sum += B[i, j] * result[j];
                    }
                    result[i] = (b[i] - sum) / B[i, i];
                }
            }

            ////
            else
            {
                for (int i = b.Length - 1; i >= 0; --i)
                {
                    double sum = 0;
                    for (int j = i + 1; j < A.GetLength(1); ++j)
                    {
                        sum += A[i, j] * result[j];
                    }
                    result[i] = (b[i] - sum) / A[i, i];
                }
            }
            
            return result;
        }

        public void saveResult(double[] b) //збереження результатів до файлу
        {
            saveFileDialog1.Filter = "txt files (*.txt)|*.txt";
            saveFileDialog1.RestoreDirectory = true;
            saveFileDialog1.InitialDirectory = Application.StartupPath;
           
            if (saveFileDialog1.ShowDialog() == DialogResult.OK)
            {
                try
                {
                    StreamWriter sw = new StreamWriter(saveFileDialog1.FileName.ToString());
                    sw.WriteLine("Шуканий вектор:");
                    for (int i = 0; i < b.Length; i++)
                    {
                        sw.WriteLine("x_" + (i + 1) + " = " + b[i]);
                    }
                   
                    sw.Close();
                }
                catch (Exception ex)
                {
                    MessageBox.Show("Неможливо зберегти до файлу.\n Original error: " + ex.Message);
                    return;
                }
            }
        }
    } 
}
