﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;

namespace Householder
{
    public partial class Form1 : Form
    {
        Manager menager;
        int m = 0, n=0; //количество строк и столбцов матрицы
        double[,] A;
        double[] b;

        private void numericUpDown1_ValueChanged(object sender, EventArgs e)
        {
            
            m = (int)numericUpDown1.Value; //размер матрицы
            n = (int)numericUpDown2.Value;

            
            dataGridView1.RowCount = m;
            dataGridView2.RowCount = m;
            dataGridView3.RowCount = n;
              

        }

        private void numericUpDown2_ValueChanged(object sender, EventArgs e)
        {
           
            
            n = (int)numericUpDown2.Value; //размер матрицы
            dataGridView1.ColumnCount = n;
            for (int i = 0; i < n; i++)
            {
               
                dataGridView3.RowCount = n;
                
                var ccol = dataGridView1.Columns[dataGridView1.Columns.Count - 1];
                ccol.Width = 60;
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            menager = new Manager();
            A = new double[m, n];
            b = new double[m];
            for (int i = 0; i < m; i++)
            {
                for (int j = 0; j < n; j++)
                {
                    try
                    {
                        A[i, j] = Convert.ToDouble(dataGridView1[j, i].Value.ToString());
                    }
                    catch (Exception ex)
                    {
                        dataGridView1.ClearSelection(); //убираем выделение всех ячеек
                        dataGridView1.CurrentCell.Selected = true; //выделяем ячейку с ошибкой
                        MessageBox.Show("Введено невірний символ!" + ex.Message);
                        return;
                    }
                }
                try
                {
                    //for (int k = 0; k< n; k++)
                    b[i] = Convert.ToDouble(dataGridView2[0, i].Value);
                }
                catch (Exception ex)
                {
                    MessageBox.Show("Введено невірний символ у матриці b!" + ex.Message);
                    return;
                }
            }

            A = menager.reflections(A);

            //string s = "";
            //for (int i = 0; i < A.GetLength(0); ++i)
            //{
            //    for (int j = 0; j < A.GetLength(1); ++j)
            //    {
            //        s += Math.Round( A[i, j], 4).ToString() + "\t";

            //    }
            //    s += "\n";
            //}
            //MessageBox.Show(s);

            double[] result = menager.reverseGauss(A, b);
           

            //for (int i =0; i<b.Length; i++)
            for (int i = 0; i < dataGridView3.RowCount; i++)
            {
                dataGridView3[0, i].Value = Math.Round(result[i], 4).ToString();
            }

            Form2 f2 = new Form2();
            f2.ShowDialog();
            if (f2.DialogResult == DialogResult.OK)
            {
                menager.saveResult(result);
                
            }
            f2.Close();
        }

        

        private void toolStripMenuItem2_Click(object sender, EventArgs e)
        {
            numericUpDown1.Value = 1;
            numericUpDown2.Value = 1;
            numericUpDown1.Enabled = true;
            numericUpDown2.Enabled = true;

        }

        private void відкритиToolStripMenuItem_Click(object sender, EventArgs e)
        {
            openFileDialog1.InitialDirectory = Application.StartupPath;
            openFileDialog1.Filter = "txt files (*.txt)|*.txt";
            openFileDialog1.RestoreDirectory = true;

            if (openFileDialog1.ShowDialog() == DialogResult.OK)
            {
                //button1.PerformClick();
                int col = 0;
                int row = 0;
                try
                {
                    StreamReader reader = new StreamReader(openFileDialog1.FileName.ToString());
                    string s;

                    do
                    {

                        s = reader.ReadLine();
                        if (s != null)
                        {
                            string[] a = s.Split(new char[] { ' ' }, StringSplitOptions.RemoveEmptyEntries);

                            dataGridView1.Rows.Add(new System.Windows.Forms.DataGridViewRow());
                            dataGridView1.ColumnCount = a.Length-1;
                            col = 0;
                            for (int column = 0; column < a.Length-1; column++)
                            {
                                dataGridView1[column, row].Value = a[column];
                                col++;

                            }
                            dataGridView2.RowCount++;
                            dataGridView2[0, row].Value = a[a.Length-1];
                            row++;
                        }
                    } while (s != null);


                    reader.Close();
                }
                catch (Exception ex)
                {
                    MessageBox.Show("Неможливо зчитати файз з диску.\n Original error: " + ex.Message);
                    return;
                }
                numericUpDown1.Value = row;
                numericUpDown2.Value = col;
                numericUpDown1.Enabled = false;
                numericUpDown2.Enabled = false;


            }

        }

        private void зберегтиToolStripMenuItem_Click(object sender, EventArgs e)
        {
            double [] result = new double[dataGridView3.RowCount];
            for (int i = 0; i < dataGridView3.RowCount; i++)
            {
                result[i] = (double)dataGridView3[0, i].Value;
               
            }
            menager.saveResult(result);
        }

        private void вихідToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Close(); 
        }

        private void button2_Click(object sender, EventArgs e) // show martrix R (upper triangle)
        {
            string s = "";
            for (int i = 0; i < A.GetLength(0); ++i)
            {
                for (int j = 0; j < A.GetLength(1); ++j)
                {
                    s += Math.Round(A[i, j], 4).ToString() + "\t";

                }
                s += "\n";
            }
            MessageBox.Show(s);
        }

        private void button3_Click(object sender, EventArgs e)
        {
            A = new double[m, n];
            for (int i = 0; i < m; i++)
            {
                for (int j = 0; j < n; j++)
                {
                    try
                    {
                        A[i, j] = Convert.ToDouble(dataGridView1[j, i].Value.ToString());
                    }
                    catch (Exception ex)
                    {
                        dataGridView1.ClearSelection(); //убираем выделение всех ячеек
                        dataGridView1.CurrentCell.Selected = true; 
                        MessageBox.Show("Введено невірний символ!" + ex.Message);
                        return;
                    }
                }
            }
                double alpha = 0;
            for (int i = 0; i < A.GetLength(0); ++i)
            {
                for ( int j = 0; j< A.GetLength(1); j++)

                alpha += Math.Pow(A[i,j], 2);
            }
            MessageBox.Show( Math.Sqrt(alpha).ToString());
        }

        public Form1()
        {
            InitializeComponent();

            m = (int)numericUpDown1.Value;
            n = (int)numericUpDown2.Value;

            dataGridView1.RowCount = m;
            dataGridView1.ColumnCount = n;
            dataGridView2.RowCount = m;
            dataGridView2.ColumnCount = 1;
            dataGridView3.RowCount = m;
            dataGridView3.ColumnCount = 1;

            dataGridView1.AllowUserToAddRows = false; //чтобы не создавалась пустая строка
            dataGridView2.AllowUserToAddRows = false;
            dataGridView3.AllowUserToAddRows = false;

            pictureBox1.Load(@"snip_А.png");
            pictureBox2.Load(@"snip_b.png");
        }
    }
}
